app=nginx
version=1.17.3
build=2sml
homepage="http://nginx.org"
download="http://nginx.org/download/nginx-$version.tar.gz"
desc="High-performance HTTP server with support for IMAP3 and POP3 proxies"
requires="zlib pcre openssl"

prepbuilddir() {
  mkandenterbuilddir
  rm -rf $app-$version

  tar xf $srcdir/$app-$version.tar.?z*
  cd $app-$version
  fixbuilddirpermissions
}

build() {
  CC="$CC" \
  ./configure \
    --prefix="/var/lib" \
    --with-http_ssl_module \
    --with-http_v2_module \
    --with-http_stub_status_module \
    --with-http_auth_request_module \
    --with-http_gunzip_module \
    --with-http_realip_module \
    --with-http_secure_link_module \
    --with-http_slice_module \
    --with-threads \
    --with-ipv6 \
    --sbin-path=/bin/nginx \
    --conf-path=/etc/nginx/nginx.conf \
    --error-log-path=/var/log/nginx/error.log \
    --lock-path=/var/lock/nginx.lock \
    --http-log-path=/var/log/nginx/access.log \
    --http-client-body-temp-path=/var/spool/nginx\body \
    --http-fastcgi-temp-path=/var/spool/nginx/fastcgi \
    --http-proxy-temp-path=/var/spool/nginx/proxy \
    --http-scgi-temp-path=/var/spool/nginx/scgi \
    --http-uwsgi-temp-path=/var/spool/nginx/uwsgi

  make
  make install DESTDIR=$pkg

  mkdir -p $pkg/var/spool/nginx
  install -Dm 644 $srcdir/nginx.conf.sample $pkg/etc/nginx/nginx.conf.sample

  cp LICENSE $pkgdocs/

  preprunitservice -s nginx -d

  (
    cd $pkg/etc/nginx
    for conffile in fastcgi_params fastcgi.conf mime.types nginx.conf koi-utf \
      koi-win scgi_params uwsgi_params win-utf ; do
        mv $conffile $conffile.new
    done
  )

  mkfinalpkg
}

sha512sums="
e2121f346bda491adb16b4120d93f95ab260ceb35376a204769490aee0dfeeaf44eca0f5ab652a6aeecc4a95e1081a3364bcf60992aa97e2fb082b9ba1a5316b  nginx-1.17.3.tar.lz
"
