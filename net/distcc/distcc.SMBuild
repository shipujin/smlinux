app=distcc
version=3.4
build=2sml
homepage="https://distcc.github.io/"
download="https://github.com/distcc/distcc/releases/download/v$version/distcc-$version.tar.gz"
desc="Distributed C and C++ compiler daemon for parallel building"
requires="popt"

prepbuilddir() {
  mkandenterbuilddir
  rm -rf $app-$version

  tar xf $srcdir/$app-$version.tar.?z*
  cd $app-$version 
  fixbuilddirpermissions

  applypatch $srcdir/dcc_gcc_rewrite_fqn-avoid-heap-corruption.patch
}

build() {
  ./configure \
    --prefix="" \
    --sbindir=/bin \
    --sysconfdir=/etc \
    --without-libiberty \
    --without-avahi

  make
  make install DESTDIR=$pkg 

  cp COPYING $pkgdocs/

  mkdir -p $pkg/lib/distcc
  (
    cd $pkg/lib/distcc
    ln -sf ../../bin/distcc gcc
    ln -sf ../../bin/distcc cc
    ln -sf ../../bin/distcc g++
    ln -sf ../../bin/distcc c++
  )

  mkfinalpkg
}

sha512sums="
c2d7fd2baacee153f722e780b5dc3780eaf2903ceff22c3eb315905d425b984403e1abab540e8cd49829bc03a86ae984b62d31b9defb493adddac7b24f428b09  distcc-3.4.tar.lz
9bc411107021d5f621304a73b0bfdec387d1b33121a325503cf4c9c4046924338ee1b4ef093b517249c3df31cf69cd9f19377df2edd6e81be1dac50a5bd0f3d5  dcc_gcc_rewrite_fqn-avoid-heap-corruption.patch
"
