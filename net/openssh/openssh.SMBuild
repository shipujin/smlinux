app=openssh
version=8.1p1
build=1sml
homepage="https://www.openssh.com/"
download="https://cdn.openbsd.org/pub/OpenBSD/OpenSSH/portable/openssh-$version.tar.gz"
desc="Client-server implementation of the SSH protocol suite"
requires="zlib openssl"

prepbuilddir() {
  mkandenterbuilddir
  rm -rf $app-$version

  tar xf $srcdir/$app-$version.tar.?z*
  cd $app-$version
  fixbuilddirpermissions

  applypatch $srcdir/openssh-sys_param.patch

  # prevent from installing some things (keysign and maybe others) setuid.
  sed -i 's@-m 4711@-m 0750@g' Makefile.in
}

build() {
  CFLAGS="$CFLAGS -D_BSD_SOURCE -DMISSING_FD_MASK -DMISSING_NFDBITS" \
  ./configure \
    --prefix="" \
    --bindir=/bin \
    --sbindir=/bin \
    --libexecdir=/lib/ssh \
    --sysconfdir=/etc/ssh \
    --with-privsep-user=nobody --with-privsep-path=/var/empty --with-xauth="$pkg/bin/xauth" \
    --without-stackprotect --with-md5-passwords --with-mantype=man \
    --disable-strip --disable-lastlog --disable-utmp --disable-utmpx \
    --disable-wtmp --disable-wtmpx --disable-pututline \
    --disable-pututxline 

  mkdir netinet
  touch netinet/in_systm.h

  sed -i '/USE_BTMP/d' config.h
  sed -i '/USE_UTMP/d' config.h
  sed -i 's@HAVE_DECL_HOWMANY 1@HAVE_DECL_HOWMANY 0@' config.h

  make
  make install-nokeys DESTDIR="$pkg"

  cp LICENCE $pkgdocs/

  rm -f $pkg/etc/ssh/sshd_config
  cp $srcdir/sshd_config $pkg/etc/ssh/sshd_config.new
  mv $pkg/etc/ssh/ssh_config $pkg/etc/ssh/ssh_config.new

  preprunitservice -s openssh 

  mkfinalpkg
}

sha512sums="
f1069dbef01e86d671d4a6d78955995dcc3956d067d08b44a6b2f86b5805ef8304fa54ac36cf7a92365a1d919e99475c460615fe3ef678ff183015961e77366a  openssh-8.1p1.tar.lz
c5a3b4382c0f63c1246e2113ee28cb18906fefef7ddffaf98552bb14f3348e42b4e18c14e21919dddfab274a5645947b0aaad190fde514ec12410c74ffeac568  openssh-sys_param.patch
"
