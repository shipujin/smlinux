#!/bin/sh
# Credits: Mario Preksavec on slackbuilds.org
config() {
  NEW="$1"
  OLD="$(dirname $NEW)/$(basename $NEW .new)"
  # If there's no config file by that name, mv it over:
  if [ ! -r $OLD ]; then
    mv $NEW $OLD
  elif [ "$(cat $OLD | md5sum)" = "$(cat $NEW | md5sum)" ]; then
    # toss the redundant copy
    rm $NEW
  fi
  # Otherwise, we leave the .new copy for the admin to consider...
}

find etc/postfix -type f -name '*.new' \
  | while read new ; do config $new ; done

PATH=/bin
SRVDIR=/var/spool/postfix

if ! grep -q '^postfix:' /etc/passwd ; then
  busybox addgroup -S postfix
  busybox addgroup -S postdrop
  busybox adduser -h "$SRVDIR" -s /bin/false -S -D -G postfix postfix
fi

# This will set the permissions on all postfix files correctly
postfix set-permissions > /dev/null 2>&1

# Symlinks added by makepkg(8)
