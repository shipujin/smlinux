app=ppp
version=2.4.8
build=1sml
homepage="https://github.com/ppp-project/ppp"
download="https://github.com/ppp-project/ppp/archive/refs/tags/ppp-$version.tar.gz"
desc="PPP daemon for establishing connectivity over a network"
requires="libpcap openssl"

prepbuilddir() {
  mkandenterbuilddir 
  rm -rf $app-$version

  tar xf $srcdir/$app-$version.tar.?z*
  cd $app-$version
  fixbuilddirpermissions
}

build() {

  applypatch $srcdir/0011-build-sys-don-t-put-connect-errors-log-to-etc-ppp.patch
  applypatch $srcdir/fix-bound-check-eap.patch
  applypatch $srcdir/fix-paths.patch
  applypatch $srcdir/fix-pppd-magic.h.patch
  applypatch $srcdir/fix-pppd-pppoe.h.patch
  applypatch $srcdir/musl-fix-headers.patch
  applypatch $srcdir/pppd-Ignore-received-EAP-messages-when-not-doing-EAP.patch
  applypatch $srcdir/radius-Prevent-buffer-overflow-in-rc_mksid.patch

  export CFLAGS="$CFLAGS -D_GNU_SOURCE"
  sed -i "s:-O2 -pipe -Wall -g:${CFLAGS}:" pppd/Makefile.linux
  sed -i "s:-g -O2:${CFLAGS}:" pppd/plugins/Makefile.linux
  sed -i "s:-O2:${CFLAGS}:" pppstats/Makefile.linux
  sed -i "s:-O2 -g -pipe:${CFLAGS}:" chat/Makefile.linux
  sed -i "s:-O:${CFLAGS}:" pppdump/Makefile.linux
  sed -i "s:^#FILTER=y:FILTER=y:" pppd/Makefile.linux
  sed -i "s:^#HAVE_INET6=y:HAVE_INET6=y:" pppd/Makefile.linux
  sed -i "s:^#CBCP=y:CBCP=y:" pppd/Makefile.linux
  sed -i "s:^#CBCP=y:CBCP=y:" pppd/Makefile.linux
  sed -i "s:^#USE_CRYPT=y:USE_CRYPT=y:" pppd/Makefile.linux

  ./configure \
    --prefix="" \
    --localstatedir=/var \
    $builddist

  make COPTS="$CFLAGS" 
  make INSTROOT="$pkg" BINDIR="$pkg"/bin install

  cp README* $pkgdocs/

  install -Dm 644 include/net/ppp_defs.h $pkg/include/net/ppp_defs.h
  mkdir -p $pkg/etc/ppp 
  cp etc.ppp/* $srcdir/ip-*  $pkg/etc/ppp/

  cp scripts/{pon,poff} $pkg/bin/
  install -Dm 644 scripts/pon.1 $pkg/share/man/man1/pon.1

  mkfinalpkg
}

sha512sums="
de20c71f9ce25aefc41a0779902a0619690cbec5eaf5f42e8d91d1a56925aec9ece0955e5a8184538f6a33815e1aa46ddc9a7d0fe73db4881e93c57256abf729  ppp-2.4.8.tar.lz
b490971d03fef4de66b61123f80a0087270bcb88466ae8ed98ea9a08b35d4c7c46b2dadd304e2970a4206bb5760a14370d7e3873de6240119d88e927ecef840c  0011-build-sys-don-t-put-connect-errors-log-to-etc-ppp.patch
ba0c062f93400008ddf47897ac2ab6a2f5017bc7f4167d1a93dd3a5c04068a922490eb4082b0da80f0c3aea6c87fdfbca3568548724a0abc148588ab86a6df32  fix-bound-check-eap.patch
8384afb992a98a7f97b484866e6aa1b1de51e901d7837f84f7ce2beba6815591450fab43957f03b65804424c4940c59640a9cd878979240a171aa77427e9c4ff  fix-paths.patch
d1067defff79d6c9f67121a9214e41a1bcca1e3b8a345ad905d223fdb8835142bad7cc3b556a3eca509ddf51cf808741773f31f4dca74e834b612a15854a5e6b  fix-pppd-magic.h.patch
d76237c82af0a3ed7ede9e814d6849b94221f1fd15e4ee68cadd33a308a32d87d635acd14f84508c9e38a10ad0a9e96ce391044da37e217d11b89a4f6631abf7  fix-pppd-pppoe.h.patch
55642ce365a7cf7dda05366ac6e74f6badba3cc7bc980760e0a2ee7bfa768ea033c4a3880b3387e0787d719742698f627c624f890d68800344d31c0309c0374d  musl-fix-headers.patch
ce1bf3298f3f99a7de643bd070cb0e7e7b1dd9621926637ffc93fd2ef552781424ce9a68c88de6eb25dc2593d543e8e329eccc2d00982bde2493e8efb7903051  pppd-Ignore-received-EAP-messages-when-not-doing-EAP.patch
d175085eaa93ccf8ade7be4f9818efe353017da7cec41d9312ad2c6685e3763834aff76d673e9d2bb0b44336f926537569ddb86a6035ec33ab8b6a7de2340132  radius-Prevent-buffer-overflow-in-rc_mksid.patch
"
