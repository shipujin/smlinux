if ! getent group polkitd ; then
  addgroup -S -g 115 polkitd
fi

if ! getent passwd polkitd ; then
  adduser -D -S -u 115 -G polkitd -s "/bin/false" -g "Policykit Daemon Owner" -h "/etc/polkit-1" polkitd
fi

