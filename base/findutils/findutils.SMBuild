app=findutils
version=4.8.0
build=1sml
homepage="https://www.gnu.org/software/findutils/"
download="https://ftp.gnu.org/gnu/findutils/findutils-$version.tar.gz"
desc="POSIX-compliant utilities to locate files"
requires="musl"

prepbuilddir() {
  mkandenterbuilddir
  rm -rf $app-$version

  tar xf $srcdir/$app-$version.tar.?z*
  cd $app-$version
  fixbuilddirpermissions

  applypatch $srcdir/findutils.no.default.options.warnings.diff
  applypatch $srcdir/mountlist.c.patch
}

build() {
  LDFLAGS="-static" \
  ./configure \
    --prefix="" \
    --libexecdir=/lib 

  make
  make install DESTDIR=$pkg

  # Provided by mlocate
  rm -f $pkg/bin/{locate,updatedb} $pkg/share/man/man1/{locate.1,updatedb.1}

  cp COPYING $pkgdocs/

  mkfinalpkg
}

sha512sums="
45183b0dc98827564fff287f8c3bc86b9a87d37d97265199f2d31a202f97c5e7ff4bacbe412916ecfdf2be96ddfe0cea3628f1b38ae7dbcf94de0101569524b3  findutils-4.8.0.tar.lz
bacb78a89dab3fb52297c9b36178ef8c75c6d840f57508771084b355d43cfc2c77785c6361fbf694064288cfb8452095bf7552b156b059e04c4345db8e4e4a39  mountlist.c.patch
ec9853fe85374db4a83282a99ba6aea7b8a4cefcb32e83002b773c05877be4a631a75ac208e946b20a0e22b9281f56cf1a022a1f17a287aadf523b0a22e2a02a  findutils.no.default.options.warnings.diff
"
