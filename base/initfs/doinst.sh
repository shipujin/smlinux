#!/bin/sh 
config() {
  NEW="$1"
  OLD="`dirname $NEW`/`basename $NEW .new`"
  # If there's no config file by that name, mv it over:
  if [ ! -r $OLD ]; then
    mv $NEW $OLD
  elif [ "`cat $OLD | md5sum`" = "`cat $NEW | md5sum`" ]; then # toss the redundant copy
    rm $NEW
  fi
  # Otherwise, we leave the .new copy for the admin to consider...
}
config etc/mtab.new
config etc/fstab.new
config etc/inittab.new
config etc/motd.new
config etc/group.new
config etc/profile.new
config etc/hosts.new
config etc/shadow.new
config etc/passwd.new
config etc/networks.new
config etc/hostname.new
config etc/issue.new
config etc/shells.new
config etc/services.new
config etc/resolv.conf.new
config etc/rc.d/rc.start.new
config etc/rc.d/rc.stop.new
config etc/rc.d/rc.network.new
config etc/rc.d/rc.modules.new
config etc/rc.d/rc.gtk.new
config etc/rc.d/rc.local.new

# Clean up useless non-examples:
rm -f etc/mtab.new
rm -f etc/motd.new
rm -f etc/hosts.new
#rm -f etc/shadow.new
rm -f etc/networks.new
rm -f etc/hostname.new
rm -f etc/shells.new
rm -f etc/issue.new

# Also ensure ownerships/perms:
chown root.shadow etc/shadow
chmod 640 etc/shadow 
( cd . ; ln -sf bin sbin )
( cd . ; ln -sf . usr )
( cd . ; ln -sf lib lib64 )
( cd var/service ; rm -rf tty1 )
( cd var/service ; ln -sf ../../etc/service/tty1 tty1 )
( cd var/service ; rm -rf tty2 )
( cd var/service ; ln -sf ../../etc/service/tty2 tty2 )
