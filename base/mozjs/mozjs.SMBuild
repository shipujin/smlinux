app=mozjs
version=78.15.0
build=1sml
homepage="https://developer.mozilla.org/en-US/docs/Mozilla/Projects/SpiderMonkey"
download="https://ftp.mozilla.org/pub/firefox/releases/"$version"esr/source/firefox-"$version"esr.source.tar.xz"
desc="Mozilla's JavaScript engine"
requires="zip unzip yasm libevent zlib alsa-lib libpng libogg libvorbis icu libvpx hunspell python3 diffutils llvm imake libXt gtk2 dbus-glib ffmpeg"

prepbuilddir() {
  mkandenterbuilddir
  rm -rf firefox-$version

  tar xf $srcdir/firefox-"$version"esr.source.tar.?z
  cd firefox-$version
  fixbuilddirpermissions
  mkdir -p AC213
  tar xf $srcdir/autoconf-2.13.tar.gz
  cd autoconf-2.13
  ./configure --prefix="$PWD"/../AC213 --program-suffix=-2.13
  make && make install
  cd ..
  export PATH="$PWD"/AC213/bin:"$PATH"

  applypatch $srcdir/0001-silence-sandbox-violations.patch
  applypatch $srcdir/disable-jslint.patch
  applypatch $srcdir/fd6847c9416f9eebde636e21d794d25d1be8791d.patch
  applypatch $srcdir/fix-musl-build.patch
  applypatch $srcdir/fix-rust-target.patch
}

build() {
  cd js/src
  export SHELL=/bin/sh
  export PYTHON=/bin/python3
  export LDFLAGS="-Wl,-z,stack-size=1048576"
  export RUST_TARGET="$arch-unknown-linux-musl"

  cd build
  ../configure \
   --prefix="" \
   --disable-jemalloc \
   --with-system-zlib \
   --with-system-icu \
   --with-system-nspr \
   --disable-strip \
   --with-clang-path=/bin/clang \
   --with-libclang-path=/lib \
   --enable-ctypes \
   --enable-hardening \
   --enable-optimize="-O2" \
   --enable-release \
   --enable-shared-js \
   --enable-system-ffi \
   --with-intl-api \
   --enable-tests \
   --disable-debug \
   --disable-debug-symbols 

  # Go easy on the pi
  if [ "$arch" = "aarch64" ] ; then
    unset MAKEFLAGS
    MAKEFLAGS="-j2" 
  fi

  make $MAKEFLAGS
  make install DESTDIR=$pkg

  # Discard the static library in staging directory
  rm -v $pkg/lib/*.ajs

  mkfinalpkg
}

sha512sums="
602584f4c77b7a554aaa068eda5409b68eb0b3229e9c224bffb91c83c4314d25de15bd560a323626ff78f6df339c79e1ef8938c54b78ecadf4dc75c5241290ad  autoconf-2.13.tar.gz
ac3de735b246ce4f0e1619cd2664321ffa374240ce6843e785d79a350dc30c967996bbcc5e3b301cb3d822ca981cbea116758fc4122f1738d75ddfd1165b6378  firefox-78.15.0esr.source.tar.xz
f7e5bee97cfa495d491dac4b8b98e5d3081346d920700e8bb6d077543e18245e5c82201a9981036ec0bf16d9fbdd42fd76e8cf6d90bb811e7338261204020149  0001-silence-sandbox-violations.patch
4f2cb93f91e798218d83cb3ac4c60b61a3658c5b269bfe250f4c4875aedaacbd77598d8d20e3a868626e49988b2073a2404e37d6918b11def774c25db68dd08d  disable-jslint.patch
60845dcb034b2c4459c30f7d5f25c8176cf42df794e2cc0e86c3e2abb6541c24b962f3a16ca70a288d4d6f377b68d00b2904b22463108559612053d835d9bff1  fd6847c9416f9eebde636e21d794d25d1be8791d.patch
bc91c2fb15eb22acb8acc36d086fb18fbf6f202b4511d138769b5ecaaed4a673349c55f808270c762616fafa42e3b01e74dc0af1dcbeea1289e043926e2750c8  fix-musl-build.patch
c397bd594428b009d1533922a3728a0ec74403714417f4b90c38c1b7751749b0585d48e77c79efa05c6c22a0d9a8ac04d535eb5bb8deb51684852c03c05d94cd  fix-rust-target.patch
"
