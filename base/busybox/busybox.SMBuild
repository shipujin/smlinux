app=busybox
version=1.27.2
build=2sml
homepage="https://www.busybox.net/"
download="https://www.busybox.net/downloads/busybox-$version.tar.bz2"
requires="musl"
desc="Swiss army knife of embedded linux providing subsets of common UNIX utilities"

prepbuilddir() {
  mkandenterbuilddir
  rm -rf $app-$version

  tar xf $srcdir/$app-$version.tar.?z*
  cd $app-$version
  fixbuilddirpermissions

  # taken from sabotage linux
  applypatch $srcdir/busybox-blowfish.patch
  applypatch $srcdir/busybox-fdisk-sector-size.patch
  applypatch $srcdir/busybox-libbb-make-unicode-printable.patch
  applypatch $srcdir/busybox-ping.patch
}

build() {
  cp $srcdir/busybox.config .config
  make CC="$CC" 

  install -Dm 755 busybox $pkg/bin/busybox

  cp LICENSE $pkgdocs/
  
  (
    cd $pkg/bin

    for f in ntpd passwd udhcpc udhcpd route ifconfig \
      telnet telnetd tftp microcom netstat killall5 setfont \
      loadkmap wall ipcalc iostat brctl arp \
      su login init chpst sv svc runsv runsvdir halt reboot poweroff getty \
      adduser addgroup deluser delgroup mdev rev;
        do ln -s busybox "$f"; 
       done
  )

  install -Dm 755 $srcdir/udhcpc-script $pkg/etc/udhcpc-script
  install -Dm 755 $srcdir/dhclient $pkg/bin/dhclient

  mkdir -p $pkg/share/man/man8
  for f in sv runsvchdir chpst runsvdir runit runsv runit ; 
     do cp $srcdir/runit-man/$f.8 $pkg/share/man/man8/
  done

  echo "server pool.ntp.org" > $pkg/etc/ntp.conf

  mkfinalpkg
}

sha512sums="
b75f66680a0fa6fd409f5070ced57d8babd02bdf3e1a194d749a2acbc5842d2474d98e5368fe9831e5e560ed5320a914357a96b2f0a0b9653c2540f7801ea198  busybox-1.27.2.tar.lz
4380794dab5bc3e6a82b18d2556bbe647503382acb33d3c82baacae166d3a6fc877b4d07b15512678c2c9868fb28f67b43b0fd804b72e3c4ee441d9108f5c5b4  busybox-blowfish.patch
8e1c116cde322f35b2a122f0c333ef36ef06334c7c0c504a8abd7f9c805d2d3c25d455d8e20ed46e960ed6dec4b543973528c948cf24ac1e02c9730d894802a5  busybox-fdisk-sector-size.patch
bee4d54bd2e740866c0eb21777767813c5789b233ed85878a96cfd459338f710abc5107fb5abb412e22f7be895c914818729fe6453cddecd99eb3241528c0d48  busybox-libbb-make-unicode-printable.patch
1057badba69fbdf98392253ae777f92c991af35cc09f094329e509cb18e668a47f0c4b2cfc7fd698c2c93c07b02833862dbc008381b753715ef6f0904b7f9b23  busybox-ping.patch
"
