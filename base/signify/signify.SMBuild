app=signify
version=30
build=2sml
libbsdversion=0.10.0
homepage="https://github.com/aperezdc/signify"
download="https://github.com/aperezdc/signify/releases/download/v$version/signify-$version.tar.xz"
desc="Tools to cryptographically sign and verify files from OpenBSD folks"
requires="musl"

prepbuilddir() {
  mkandenterbuilddir
  rm -rf $app-$version

  tar xf $srcdir/$app-$version.tar.?z*
  cd $app-$version
  fixbuilddirpermissions
}

build() {
  # We don't need a system-wide libbsd install because it would then get sucked 
  # in by xorg and qt5-based applications. We'll keep it contained to the 
  # signify source directory by modifying the pkgconfig path and CFLAGS and LDFLAGS.

  mkdir -p libbsd && cd libbsd

  (
    tar xf "$srcdir"/libbsd-$libbsdversion.tar.?z
    cd libbsd-$libbsdversion
    ./configure --prefix="$PWD"/../
    make && make install
  )

  LIBBSDPATH="$PWD"
  cd ..

  PKG_CONFIG_PATH="$PKG_CONFIG_PATH:$LIBBSDPATH/lib/pkgconfig"
  applypatch $srcdir/sha2.h.patch
  make CC+=" -static" \
   CFLAGS="-I$LIBBSDPATH/include" LDFLAGS="-L$LIBBSDPATH/lib"
  make install PREFIX="" DESTDIR=$pkg

  cp COPYING $pkgdocs/

  mkfinalpkg
}

sha512sums="
b75529785b16c93d31401187f8a58258fbebe565dac071c8311775c913af989f62cd29d5ce2651af3ea6221cffd31cf04826577d3e546ab9ca14340f297777b9  libbsd-0.10.0.tar.xz
1bc9bb5eff5575af046978df34693a2e0a1d3c275d8cb88b38d42264a2b550e25cce4951b76ced97ac65ff7d32ccac52541dc05d75fae28907ab798aec73e58f  signify-30.tar.lz
15d8eaa27c6e46862d8957341501c9f0dab254ed9652aca130c47b7478c025796d90b7ded10a74b69020c8da550b167a65b471b1c01d207abe0d281a785e6ffe  sha2.h.patch
"
