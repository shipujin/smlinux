app=rxvt-unicode
version=9.22
build=1sml
homepage="https://software.schmorp.de/pkg/rxvt-unicode.html"
download="https://dist.schmorp.de/rxvt-unicode/Attic/rxvt-unicode-$version.tar.bz2"
desc="Enhanced version of rxvt terminal emulator with full support for unicode and XFT"
requires="gcc-libs netbsd-curses glib perl libxft startup-notification"

prepbuilddir() {
  mkandenterbuilddir
  rm -rf $app-$version

  tar xf $srcdir/$app-$version.tar.?z*
  cd $app-$version
  fixbuilddirpermissions
}

build() {
  ./configure \
    --prefix="" \
    --enable-everything \
    --enable-256-color \
    --enable-unicode3 \
    --enable-xft \
    --enable-font-styles \
    --enable-transparency \
    --enable-fading \
    --enable-frills \
    --enable-pixbuf \
    --enable-rxvt-scroll \
    --enable-next-scroll \
    --enable-xim \
    --enable-iso14755 \
    --enable-keepscrolling \
    --enable-smart-resize \
    --enable-text-blink \
    --enable-pointer-blank \
    --disable-wtmp \
    --disable-lastlog 

  make
  make install DESTDIR=$pkg

  cp COPYING $pkgdocs/

  mkfinalpkg
}

sha512sums="
1316746ba1771dd6e6c306832227227ab6685aad1a3e246d4059fd60ecf36089aeee12a8d28a376e70ccc148b7e6457bcded6dad4531bbed6af3ab8c6805ec03  rxvt-unicode-9.22.tar.lz
"
