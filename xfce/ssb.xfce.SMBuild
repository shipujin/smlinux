#!/bin/bash
# Version: 1.7 GSB Section SMBuild - Do not remove this line!
# Copyright (c) 2007, 2008:
#   Darren 'Tadgy' Austin <darren (at) gnomeslackbuild.org>, Coventry, UK.
# Copyright (c) 2019-2022 PktSurf <smlinux@pktsurf.in>
# Licenced under the terms of the GNU General Public Licence version 3.
#
# Modified and trimmed extensively for use with SMLinux distribution
# http://git.pktsurf.in/smlinux

# Prevent users from directly executing this section autobuild file. The whole build 
# process has to be initiated from the main autobuild file.
if [ -z "$autobuild" ] ; then
  echo "Please invoke the main ssb.SMBuild file rather than this section build file because"
  echo "it has the required functions that are exported to this section build file during"
  echo "the build process"
  exit 1
fi

. /etc/bldpkg.conf

if [ -n "$autobuildtemp" ] ; then
  autobuildtemp="$autobuildtemp"
  export autobuildtemp
fi

colours=0
export colours

# Make sure we are in the right directory (you can never trust users..)
cd $( cd ${BASH_SOURCE%/*} ; pwd )

# Section name.
# This should not need to be changed unless the auto detection fails.
section="$( basename $( pwd ) )"
export section

if [ ! -f .buildlist."$section" ]; then
	echo ""
	echo "**********************************************************************"
	echo "The buildlist either doesn't exist, or is of a different architecture."
	echo "** .buildlist.$section is needed **"
	echo "Exiting!"
	echo "**********************************************************************"
	exit 1
fi

# Packages to build.
# The package list is read in from .buildlist in the current directory, with
# any comments and blank lines removed.
packages="$( egrep -v "^#|^$" .buildlist."$section" | cut -d'#' -f1 )"

list_packages() {
  local package
  echo "The following packages are built in this section, listed in processing order:"

  ( for package in $packages
    do
      echo -n "$package, "
    done ) | sed -e 's/, $//' | fmt -w 74 | sed -e 's/^/    /g'
}

find_package_files() {
  # $1 = Directory to look for files in [required]
  # $2 = Package name or regex to match.  An empty string matches all.
  # $3 = Package version or regex to match.  An empty string matches all.
  # $4 = Package architecture or regex to match.  An empty string matches all.
  # $5 = Package build tag or regex to match.  An empty string matches all.
  # $6 = File extension or regex to match.  An empty string means no extension.
  # Note: Remember to escape any regex characters used in fixed strings.

  [ -z "$1" ] || [ ! -d "$1" ] && return 1
  find $1 -maxdepth 1 -mindepth 1 2>/dev/null | \
    egrep "^.*/(${2:-.*})(-${3:-[^-]*})(-${4:-[^-]*})(-${5:-[^-.]*})($6)$" 2>/dev/null
  return $?
}

# Environment.
packagesdir=${packagesdir:-/$arch}
export packagesdir

# Option defaults.
nopatchesdir=0
noinstall=0

# This check compares a list of source directories with the list of the
# packages in the build list and warns of any missing package names 
# in either of the two.

dirtempfile=$(mktemp $parenttmp/DIRECTORYNAMES."$section".XXXXXX)
dirfiletemppath="$dirtempfile"
dirlist=$(find . -type d -maxdepth 1 -mindepth 1 | sed 's@./@@' | sort > $dirfiletemppath)

packtempfile=$(mktemp $parenttmp/BUILDFILENAMES."$section".XXXXXX)
packfiletemppath="$packtempfile"
sort .buildlist.$section > $packfiletemppath
  
directorycount="$( wc -l < $dirtempfile )"
buildlistcount="$( wc -l < $packtempfile )"

# Get number of total packages
totalpkgnumber="$(wc -l < .buildlist.$section)"
export totalpkgnumber

if diff -u "$dirfiletemppath" "$packfiletemppath" > /dev/null 2>&1 ; then
  diffstatus="0"
else
  diffstatus="1"
fi

if [ "$diffstatus" != "0" ]; then 
  echo "*********************************************************************"
  echo "** Warning: In section '"$section"', the number of packages in the"
  echo "** hidden file '.buildlist."$section"' is different to the number of"
  echo "** package directories. Some packages may not have been added to"
  echo "** this file/section directory. They are listed below:"
  echo ""

  diff -u "$dirfiletemppath" "$packfiletemppath" || true
  echo ""
  diff -u "$packfiletemppath" "$dirfiletemppath" || true

  echo ""
  echo "** Building anyways :-) "
  echo "*********************************************************************"
  sleep 2

fi
rm -f $packfiletemppath $dirfiletemppath

# Parse command line arguments.
while [ $# -gt 0 ]; do
  if [ "$1" = "-help" ] || [ "$1" = "--help" ]; then
    usage
    exit 0
  elif [ "$1" = "-list" ] || [ "$1" = "--list" ]; then
    list_packages
    exit 0
    shift
  else
    echo "${0##*/}: Unknown option: $1"
    echo "Try: $0 --help"
    exit 1
  fi
done

# Temporary space, package and log file storage.
mkdir -p $pkgdest $logsdir/$section

      echo "*********************************************************************"
      echo "** Building section '$section'..."
      echo "*********************************************************************"

# Process packages.
( for package in $packages
  do
    # Build defaults.
    skip_build=0
    subdir=$packagesdir/$section

      echo "*********************************************************************"
      echo "*** Processing package '$package'..."
      echo "*********************************************************************"

    # Sanity checks.
    [ ! -e "$package/$package.SMBuild" ] && {
      echo "*********************************************************************"
      echo "*** Error: '$package.SMBuild' not found."
      echo "*********************************************************************"
      exit 1
    }
    [ ! -x "$package/$package.SMBuild" ] && {
      echo "*********************************************************************"
      echo "*** Error: '$package.SMBuild' is not executable."
      echo "*********************************************************************"
      exit 1
    }

    # Get package version and build numbers from the package SMBuild.
    source "$package/$package.SMBuild"

    # Check that we got a version and build.
    [ -z "$version" ] || [ -z "$build" ] && {
      echo "*********************************************************************"
      echo "*** Error: failed to get version or build from '$package.SMBuild'"
      echo "*********************************************************************"
      exit 1
    }

    # Check if the package should be rebuilt, and where it should be put.
    # The assumption is to always rebuild and put packages in the main
    # directory, unless modified by the checks below.
    if find_package_files "$pkgdest/$subdir" "${package//+/\+}" \
      "" "" "" "\.$pkgext" >/dev/null
    then
      if find_package_files "$pkgdest/$subdir" "${package//+/\+}" \
        "${version//-/_}" "" "$build" "\.$pkgext" >/dev/null
      then
        # Package with same version/build was found in the main directory.
        skip_build=1
      fi
    fi

    # Build package if required.
    if [ "$skip_build" = "0" ]; then
      echo "*********************************************************************"
      echo "*** Building package '$package'..."
      echo "*********************************************************************"
      # Get the current package number from the build list
      currentpkgnumber="$(grep -Ewn "^$package" .buildlist.$section | cut -d: -f 1)"
      export currentpkgnumber
      mkdir -p $pkgdest/$subdir
      ( cd $package && export pkgdest=$pkgdest/$subdir &&
        bldpkg 2>&1 ) | \
        tee $logsdir/$section/$package-$section-$HOSTTYPE.log.txt
      # Unset $CURRENTPKGNUMBER. We don't want issues when a new one comes in
      err=${PIPESTATUS[0]}
      if [ "$err" != "0" ] ; then 
        unset currentpkgnumber
        echo "*** Error: '$package' build failed."
        exit $err
      else
        unset currentpkgnumber
        mv $logsdir/$section/$package-$section-$HOSTTYPE.log.txt \
          $logsdir/$section/$package-$version-$build-$section-$HOSTTYPE.log.txt
      fi
    else
      echo "*********************************************************************"
      echo "*** Skipping build of '$package' - package up to date."
      echo "*********************************************************************"

    fi

    if [ "$noinstall" = "0" ]; then
      echo
      echo "*********************************************************************"
      echo "*** Installing '$package'..."
      echo "*********************************************************************"
      upgradepkg --install-new $( find_package_files "$pkgdest/$subdir" \
        "${package//+/\+}" "${version//-/_}" "" "$build" "\.$pkgext" ) || {
        echo
        echo "*********************************************************************"
        echo "*** Error: failed to install '$package'."
        echo "*********************************************************************"
        exit 1
      }
    else
      echo
      echo "*********************************************************************"
      echo "*** Warning: not installing '$package'."
      echo "*********************************************************************"
    fi
  done

  echo "*********************************************************************"
  echo "** Finished building section '$section'."
  echo "** SMLinux packages are in '$pkgdest/$arch/$section'."
  echo "** Section build logs are in '$logsdir/$section'."
  echo "*********************************************************************"
  echo "** Section build time was $( runtime $SECONDS )"
  echo "*********************************************************************"
) 2>&1 | tee $logsdir/$section-$arch.log.txt

# Return the exit status from the sub-shell, not the tee command.
exit ${PIPESTATUS[0]}
