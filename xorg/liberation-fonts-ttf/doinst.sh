#!/bin/sh
# Update the X font indexes:
if [ -x /bin/mkfontdir ]; then
  mkfontscale share/fonts/TTF 2> /dev/null
  mkfontdir share/fonts/TTF 2> /dev/null
fi
if [ -x /bin/fc-cache ]; then
  /bin/fc-cache -f 2> /dev/null
fi
