#!/bin/sh
# Update the X font indexes:
if [ -x /bin/mkfontdir -o -x /X11R6/bin/mkfontdir ]; then
  ( cd /share/fonts/misc
    mkfontdir .
  )
fi
if [ -x /bin/fc-cache ]; then
  /bin/fc-cache -f
fi
