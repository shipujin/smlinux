config() {
  NEW="$1"
  OLD="$(dirname $NEW)/$(basename $NEW .new)"
  # If there's no config file by that name, mv it over:
  if [ ! -r $OLD ]; then
    mv $NEW $OLD
  elif [ "$(cat $OLD | md5sum)" = "$(cat $NEW | md5sum)" ]; then # toss the redundant copy
    rm $NEW
  fi
  # Otherwise, we leave the .new copy for the admin to consider...
}

# The GTK+3 theme no longer works, so if gtkrc uses that, back up the old
# file and install the new one:
if grep -q GTK etc/gtk-3.0/gtkrc 2> /dev/null ; then
  mv etc/gtk-3.0/gtkrc etc/gtk-3.0/gtkrc.bak
fi
config etc/gtk-3.0/gtkrc.new
config etc/gtk-3.0/im-multipress.conf.new
rm -f etc/gtk-3.0/gtkrc.new

rm -f /usr/share/icons/*/icon-theme.cache >/dev/null 2>&1
[ -x /etc/rc.d/rc.gtk ] && /etc/rc.d/rc.gtk
